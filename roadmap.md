cache solution:
    http server's native cache solution: e. g apache httpd.conf;
    linux kernal?;
    programming language's module:
            java: jbossCache/treeCache;
            golang: go-cache go2cache;
    load balance;
    database cluster: 
            LBC, HPC, HAC;
            e .g of mysql: m-m-slave as a cluster, in addtion to a global router database \
            p.s: auto_increment is forbbiden;none pconnect(php);
    to sperate image server;
    库表散列;

interview map:
    fundamental: 
        data structure: b-tree, b+tree, red-black, tree;
        algorithms: 
            sort: heapsort, selectionsort, insertionsort, quiksort, bubblesort
    network: socket, rpc
    facility:
        os: linux kernal;
        database:
            sql or nosql, 
            disk and memory;
        compiler
    concepts: 
        multi-process: 
            concurrency, parallelism,
            unit: process, threads, coroutine
    programming language category: compiler, interpreter, virtual machine 
    golang:
        goroutine: 
            Communicating Sequential Process(CSP);
            synchronization:
                low-level: sync.Mutex, sync.WaitGroup;
                high-level: 
                    channel:
                        select and timeout:
                            dafault: time.After()
                dataProducer and dataReceiver:
                    close(channel ch): return zore value and false
        inheritance: can not overide
    middleware: message cache search filestore
    单体应用:ERP CRM SCM HR PLM MES
# future value of ordinary annuity
```math
fv = c(1+r)^n-1 + ... c
     // let a = c and q = 1+r
     =  a + aq + ... aq^(n-1)
     = a(1 + q + ... q^(n-1))


sn = aq + aq^2 + aq^n
sn - qsn = a(q + ... + q^n) - a(q^2...)
         = a(q - q^(n+1))
sn(1-q)  = aq(1-q^n)
sn       = aq(1-q^n) / (1-q)
         = a(1+r)((1+r)^n - 1) / r

fn = sn /q = a(1-q^n) / (1-q) 
           = c(1-(1+r)^n) / (-r)
           =  c((1+r)^n - 1) / r      
```
# future value interest factor of annuity
```math
fvifa = ( (1 + r)^n - 1 ) / r 
```
